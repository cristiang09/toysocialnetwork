package service;
import domain.Friendship;
import domain.Network;
import domain.Tuple;
import domain.User;
import domain.validators.ValidareException;
import domain.validators.ValidareFriendship;
import domain.validators.ValidareUser;
import domain.validators.Validator;
import repository.Repository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ServiceUF {


    private final Repository<Tuple<Long>, Friendship> repositoryFriendship;
    private final Repository<Long, User> repositoryUser;

    /**
     * Constructorul clasei ServiceUF
     * @param repositoryFriendship FriendshipFileRepository
     * @param repositoryUser UserFileRepository
     */
    public ServiceUF(Repository<Tuple<Long>, Friendship> repositoryFriendship, Repository<Long, User> repositoryUser)
    {

        this.repositoryFriendship = repositoryFriendship;
        this.repositoryUser = repositoryUser;
    }

    /**
     * Metoda valideaza si creeaza un User in cazul in care datele sunt valide
     * @param attr List<String>, lista de atribute
     * @return User
     * @throws ValidareException
     */
    private User createValidateUser(List<String> attr)
    {
        Validator<Long, User> validatorUser = new ValidareUser(repositoryUser.findAll());
        User user = new User(attr.get(0), attr.get(1));
        validatorUser.validate(user);
        return user;
    }

    /**
     * Metoda adauga un user, in cazul in care datele sunt valide.
     * @param attr List<String>
     * @return User, user-ul adaugat
     * @throws ValidareException
     */
    public User addUser(List<String> attr)
    {
        User user = createValidateUser(attr);
        repositoryUser.save(user);
        return user;
    }

    /**
     * Metoda creeaza si valideaza un Friendship.
     * @param attr List<String>
     * @return Friendship
     * @throws ValidareException
     */
    private Friendship createValidateFriendship(List<String> attr)
    {
        Validator<Tuple<Long>, Friendship> validatorFriendship = new ValidareFriendship(repositoryFriendship.findAll(), repositoryUser.findAll());
        Date date = new Date();
        Friendship friendship = new Friendship(date.toString());
        Tuple<Long> id = new Tuple<>(Long.parseLong(attr.get(0)), Long.parseLong(attr.get(1)));
        friendship.setId(id);
        validatorFriendship.validate(friendship);
        return friendship;
    }

    /**
     * Metoda adauga un friendship in cazul in care ste valid
     * @param attr List<String>
     * @return frienship
     */
    public Friendship addFriendship(List<String> attr)
    {
        Friendship friendship1 = createValidateFriendship(attr);
        repositoryFriendship.save(friendship1);
        this.addFriendsFromFriendship(friendship1);
        return friendship1;
    }

    /**
     * @return toate valoriile user din map
     */
    public Iterable<User> getAllUsers() {
        Iterable<User> allUsers = repositoryUser.findAll();
        allUsers.forEach(user->{
            listAllIdFriendsForOneUser(user.getId()).forEach(id->{user.setFriends(findOneUser(id));});
        });
        return allUsers;
    }

    public List<Long> listAllIdFriendsForOneUser(long id)
    {
        List<Long> listFriends = new ArrayList<>();
        for (Friendship friendship : getAllFriendships())
        {
            if (friendship.getId().getFirstTupleElement().equals(id)) listFriends.add(friendship.getId().getSecondTupleElement());
            if (friendship.getId().getSecondTupleElement().equals(id)) listFriends.add(friendship.getId().getFirstTupleElement());
        }
        return listFriends;
    }

    /**
     * @return toate friendship-urile din map.
     */
    public Iterable<Friendship> getAllFriendships() {return repositoryFriendship.findAll();}

    /**
     * Metoda cauta un user dupa id.
     * @param Id long
     * @return E, daca entitatea exista
     *         null, daca entitatea nu exista
     * @throws IllegalArgumentException, daca id-ul este null
     */
    public User findOneUser(long Id){return repositoryUser.findOne(Id);}

    /**
     * Metoda cauta un friendship dupa id.
     *      * @param Id Tuple<Long> Id
     *      * @return Friendship, daca entitatea exista
     *      *         null, daca entitatea nu exista
     *      * @throws IllegalArgumentException, daca id-ul este null
     */
    public Friendship findOneFriendship(Tuple<Long> Id){return repositoryFriendship.findOne(Id);}

    /**
     * Metoda sterge un user si toate prieniile acestuia.
     * @param Id long
     * @return null daca stergerea se realizeaza
     *         E, entitatea care a fost stearsa
     * @throws IllegalArgumentException, in cazul in care Id-ul este null
     */
    public User removeUser(long Id)
    {
        Validator<Long, User> validatorUser = new ValidareUser(repositoryUser.findAll());
        validatorUser.deleteValidate(Id);
        List<Tuple<Long>> listFriends = this.friendsForOneUser(Id);
        listFriends.forEach(this::removeFriendship);
        return repositoryUser.delete(Id);
    }

    /**
     * Metoda sterge o prietenie si pritenii de la cei 2 useri.
     * @param Id Tuple<Long>
     * @return null daca stergerea se realizeaza
     *         E, entitatea care a fost stearsa
     * @throws IllegalArgumentException, in cazul in care Id-ul este null
     */
    public Friendship removeFriendship(Tuple<Long> Id)
    {
        Validator<Tuple<Long>, Friendship> validatorFriendship = new ValidareFriendship(repositoryFriendship.findAll(), repositoryUser.findAll());
        validatorFriendship.deleteValidate(Id);
        findOneUser(Id.getFirstTupleElement()).getFriends().remove(Id.getSecondTupleElement());
        findOneUser(Id.getSecondTupleElement()).getFriends().remove(Id.getFirstTupleElement());
        return repositoryFriendship.delete(Id);
    }

    /**
     * Metoda adauga, plecand de la prietenii, user prieteni pentru fiecare user.
     * @param friendship Friendship
     */
    private void addFriendsFromFriendship(Friendship friendship)
    {
        long id1 = friendship.getId().getFirstTupleElement();
        long id2 = friendship.getId().getSecondTupleElement();
        findOneUser(id1).setFriends(findOneUser(id2));
        findOneUser(id2).setFriends(findOneUser(id1));
    }

    /**
     *  * Metoda adauga, plecand de la prietenii, user prieteni pentru fiecare user.
     */
    private void addFriendsFromFriendshipInService()
    {
        getAllFriendships().forEach(this::addFriendsFromFriendship);
    }

    /**
     * Metoda realizeaza lista de prieteni a unui User.
     * @param Id long
     * @return List<Tuple<Long>> lista de prieteni
     */
    private List<Tuple<Long>> friendsForOneUser(long Id)
    {
        List<Tuple<Long>> listFriends = new ArrayList<>();
        for (Friendship friendship : getAllFriendships()){
            if (friendship.getId().getFirstTupleElement().equals(Id)) listFriends.add(friendship.getId());
            else if (friendship.getId().getSecondTupleElement().equals(Id)) listFriends.add(friendship.getId());}
        return listFriends;
    }

    /**
     * Metoda creaza lista de adiacenta necesare retelei.
     * @param network Retea
     */
    public void createAdjList(Network network)
    {
        for(Friendship friendship : getAllFriendships())
        {
            network.addEdge(findOneUser(friendship.getId().getFirstTupleElement()), findOneUser(friendship.getId().getSecondTupleElement()));
        }
    }

    /**
     * Metoda calculeaza numarul de comunitati ale unei retele
     * @return int, numarul de comunitati ale unei retele
     */
    public int numberOfConnectedComponents()
    {
        List<User> users = new ArrayList<>();
        repositoryUser.findAll().forEach(users::add);
        Network retea = new Network(users);
        this.createAdjList(retea);
        return retea.numberOfConnectedComponents();
    }

    /**
     * Metoda realizeaza o lista de user, care reprezinta comunitatea maxima.
     * @return List<User>, comunitatea maxima
     */
    public List<User> maxConnectedComponent()
    {
        List<User> users = new ArrayList<>();
        repositoryUser.findAll().forEach(users::add);
        Network retea = new Network(users);
        this.createAdjList(retea);
        return retea.maxConnectedComponent();
    }

    public void updateUser(long id, String firstName, String lastName)
    {
        User user = new User(firstName, lastName);
        user.setId(id);
        repositoryUser.update(user);
    }
}
