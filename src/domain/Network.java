package domain;

import java.util.*;

public class Network {
    private final List<List<Long>> adjList;
    private List<User> Users;
    private Map<User, Long> mapUsers;

    /**
     * Constructorul clasei Retea
     * @param listUsers List<User>
     */
    public Network(List<User> listUsers)
    {
        this.adjList = new ArrayList<>();
        this.Users = listUsers;
        this.mapUsers = new HashMap<>();
        for (int i = 0; i < Users.size(); i++) {
            adjList.add(i, new ArrayList<>());
            mapUsers.put(listUsers.get(i), ((long) i));
        }
    }

    private void initAdjListAndMapUsers()
    {
        for (int i = 0; i < Users.size(); i++) {
            adjList.add(i, new ArrayList<>());
            mapUsers.put(Users.get(i), ((long) i));

        }
    }

    /**
     * Metoda adauga in lista de adiacenta o prietenie.
     * @param user1 User
     * @param user2 User
     */
    public void addEdge(User user1, User user2)
    {
      adjList.get(mapUsers.get(user1).intValue()).add(mapUsers.get(user2));
      adjList.get(mapUsers.get(user2).intValue()).add(mapUsers.get(user1));
    }


    /**
     * Metoda aplica DFS
     * @param v int, nodul sursa
     * @param visited boolean[], lista a caror valori sunt true daca dfs a trecut prin acel nod,
     *                  false in caz contrar
     * @param cmp lista de useri prin care s-a trecut
     */
    public void DFS(int v, boolean[] visited, List<User> cmp)
    {
        visited[v] = true;
        cmp.add(Users.get(v));
        for (long x : adjList.get(v)) {
            if (!visited[((int) x)])
                DFS(((int) x), visited,cmp);
        }
    }


    /**
     * Metoda determina numarul de comunitati dintr-o retea.
     * @return int
     */
    public int numberOfConnectedComponents()
    {
        List<User> resultUsersList= new ArrayList<>();
        boolean[] visited = new boolean[Users.size()];
        int numberConnected = 0;
        for (int i = 0; i < Users.size();i++)
        {
            if (!visited[i])
            {
                DFS(i, visited, resultUsersList);
                numberConnected++;
            }
        }
        return numberConnected;
    }

    /**
     * Metoda determina cea mai mare comunitate din retea.
     * @return List<User>
     */
    public List<User> maxConnectedComponent()
    {
        List<User> finalResult= new ArrayList<>();
        boolean[] visited = new boolean[Users.size()];
        for (int i = 0; i < Users.size();i++)
        {
            if (!visited[i])
            {
                List<User> result = new ArrayList<>();
                DFS(i, visited, result);
                if (finalResult.size() < result.size()) finalResult = result;
            }
        }
        return finalResult;
    }
}
