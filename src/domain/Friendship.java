package domain;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

public class Friendship extends Entity<Tuple<Long>>{
    private String dateFriendship;

    public Friendship(String dateFriendship) {
        this.dateFriendship = dateFriendship;
    }

    public String getDateFriendship() {
        return dateFriendship;
    }

    @Override
    public String toString()
    {
        return this.getId().getFirstTupleElement().toString() + ", " + this.getId().getSecondTupleElement().toString();
    }

    @Override
    public int hashCode() {return Objects.hash(this.getId());}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Friendship)) return false;
        Friendship that = (Friendship) o;
        return ((this.getId().getFirstTupleElement().equals(((Friendship) o).getId().getFirstTupleElement())) ||
                (this.getId().getSecondTupleElement().equals(((Friendship) o).getId().getSecondTupleElement())));
    }

}
