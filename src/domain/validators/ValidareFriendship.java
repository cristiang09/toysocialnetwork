package domain.validators;
import domain.Friendship;
import domain.Tuple;
import domain.User;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class ValidareFriendship implements Validator<Tuple<Long>, Friendship>{
    private final Iterable<Friendship> listFriendship;
    private final Iterable<User> listUser;

    /**
     * Constructor clasa ValidareFriendship
     * @param listFriendship Iterable<Friendship>
     * @param listUser  Iterable<User>
     */
    public ValidareFriendship(Iterable<Friendship> listFriendship, Iterable<User> listUser)
    {
        this.listFriendship = listFriendship;
        this.listUser = listUser;
    }


    @Override
    public void validate(Friendship friendship) throws ValidareException {
        if (friendship == null) throw new ValidareException("ValidareException-> Friendship nu poate fi null!");
        if (friendship.getId().getFirstTupleElement() == null || friendship.getId().getSecondTupleElement() == null)
            throw new ValidareException("ValidareException-> Id-urile prietenilor nu poate null!");
        Tuple<Long> id = friendship.getId();
        listFriendship.forEach(friendship1 -> {
            if (friendship1.getId().equals(id)) throw new ValidareException("ValidareException-> Prietenie existenta!");
        });
        validateExistentIdForFriendship(id);
    }

    @Override
    public void deleteValidate(Tuple<Long> longTuple) throws ValidareException {
        AtomicBoolean condition = new AtomicBoolean(false);
       listFriendship.forEach(friendship -> {if (friendship.getId().equals(longTuple)) condition.set(true);});
       if (!condition.get()) throw new ValidareException("ValidareException-> Prietenia nu exista!");
    }

    private void validateExistentIdForFriendship(Tuple<Long> id) throws ValidareException
    {
        AtomicBoolean condition1 = new AtomicBoolean(false);
        AtomicBoolean condition2 = new AtomicBoolean(false);
        listUser.forEach(user -> {if (user.getId().equals(id.getFirstTupleElement())) condition1.set(true);});
        listUser.forEach(user -> {if (user.getId().equals(id.getSecondTupleElement())) condition2.set(true);});
        if ((!condition1.get()) || (!condition2.get())) throw new ValidareException("ValidareException-> Cel putin un ID nu exista in lista de User!");
    }
}
