package domain.validators;
import domain.User;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class ValidareUser implements Validator<Long, User>{
    private final Iterable<User> UserContainer;

    /**
     * Constructorul clasei ValidareUser
     * @param userContainer Iterable<User>
     */
    public ValidareUser(Iterable<User> userContainer)
    {
        this.UserContainer = userContainer;
    }

    @Override
    public void validate(User user) throws ValidareException {
        if (user == null) throw new ValidareException("ValidareException->User-ul nu poate fi null!");
        if (user.getFirstName().equals("") || user.getLastName().equals("")) throw new ValidareException("ValidareException->Numele nu poate fi vid!");
    }

    @Override
    public void deleteValidate(Long id) throws ValidareException{
        AtomicBoolean condition = new AtomicBoolean(false);
        UserContainer.forEach(user -> {if (user.getId().equals(id)) condition.set(true);});
        if (!condition.get()) throw new ValidareException("ValidareException->Id-ul nu exista!");
    }


}
