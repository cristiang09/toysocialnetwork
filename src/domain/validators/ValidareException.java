package domain.validators;

public class ValidareException extends RuntimeException{

    public ValidareException() {
    }

    public ValidareException(String message) {
        super(message);
    }

    public ValidareException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidareException(Throwable cause) {
        super(cause);
    }

    public ValidareException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
