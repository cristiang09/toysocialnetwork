package domain.validators;
import domain.Entity;
import domain.User;
import java.util.List;

public interface Validator<ID, V extends Entity<ID>> {
    /**
     * Metoda valideaza entiatea.
     * @param entity List<String>
     * @throws ValidareException
     */
    void validate(V entity) throws ValidareException;

    void deleteValidate(ID id) throws ValidareException;
}
