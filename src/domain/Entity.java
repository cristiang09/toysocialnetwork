package domain;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

public class Entity<ID> implements Serializable {

    private ID Id;
    @Serial
    private static final long serialVersionUID = 12345678910L;

    /**
     * @return ID
     */
    public ID getId(){return this.Id;}

    /**
     * Metoda seteaza id-ul unei entitati
     * @param id-> ID
     */
    public void setId(ID id){this.Id = id;}
}
