package domain;

import java.util.*;

public class User extends Entity<Long>{
    private String firstName;
    private String lastName;
    private final Map<Long, User> friends;

    /**
     * Constructorul clasei User.
     * @param firstName String
     * @param lastname String
     */
    public User(String firstName, String lastname)
    {
        this.firstName = firstName;
        this.lastName = lastname;
        this.friends = new HashMap<>();
    }

    /**
     * @return String
     */
    public String getFirstName(){return this.firstName;}

    /**
     * @return String
     */
    public String getLastName(){return this.lastName;}

    /**
     * @return Map<Long, User>
     */
    public Map<Long, User> getFriends(){return this.friends;}

    /**
     * Metoda adauga un user in lista de prieteni a unui user.
     * @param user User
     */
    public void setFriends(User user){this.friends.put(user.getId(), user);}

    /**
     * Metoda seteaza firstName-ul unui user.
     * @param firstName String
     */
    public void setFirstName(String firstName){this.firstName = firstName;}

    /**
     * Metoda seteaza lastName-ul unui user
     * @param lastname String
     */
    public void setLastName(String lastname){this.lastName = lastname;}

    /**
     * Metoda realizeaza un mod de afisare a unei liste de prieteni.
     * @param listUsers Map<Long, User>
     * @return String
     */
    public String friendsToString(Map<Long, User> listUsers)
    {
        String resultString = "";
        for (User user : listUsers.values())
        {
            resultString = resultString + "\n\t\t" + user.getFirstName() + "  " + user.getLastName();
        }
        return resultString;
    }

    @Override
    public String toString()
    {
        return "\n<-Utilizator->\n" +
                "\tfirstName-> " + this.firstName + '\n' +
                "\tlastName-> " + this.lastName + '\n'+
                "\t<-FRIENDS-> " + friendsToString(this.friends);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User that = (User) o;
        return getFirstName().equals(that.getFirstName()) &&
                getLastName().equals(that.getLastName()) &&
                getFriends().equals(that.getFriends());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getFriends());
    }
}
