package domain.dto;

import domain.User;

public class UserDTO {
    private  String firstName;
    private  String lastName;

    public UserDTO(String firstName, String lastName)
    {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString()
    {
        return firstName + "  " + lastName;
    }
}
