package domain.dto;

import domain.User;

public class FriendshipDTO {
    private UserDTO userDTO1;
    private UserDTO userDTO2;

    public FriendshipDTO(UserDTO user1, UserDTO user2)
    {
        this.userDTO1 = user1;
        this.userDTO2 = user2;
    }

    public UserDTO getUserDTO1() {
        return userDTO1;
    }

    public UserDTO getUserDTO2(){
        return userDTO2;
    }

    @Override
    public String toString()
    {
        return "Prieteni: " + userDTO1.toString() + ", " + userDTO2.toString();
    }
}
