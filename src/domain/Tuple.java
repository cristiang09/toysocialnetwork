package domain;

import java.util.Objects;

public class Tuple <E extends Comparable<E>> {
    private E tupleFirst;
    private E tupleSecond;

    /**
     * Constructorul clasei Tuple
     * @param tupleFirst E
     * @param tupleSecond E
     */
    public Tuple(E tupleFirst, E tupleSecond)
    {
        this.tupleFirst = tupleFirst;
        this.tupleSecond = tupleSecond;
    }

    /**
     * @return E
     */
    public E getFirstTupleElement() { return this.tupleFirst; }

    /**
     * @return E
     */
    public E getSecondTupleElement() { return this.tupleSecond; }

    /**
     * Metoda seteaza o valoare primului element din tuplu.
     * @param tupleFirst E
     */
    public void setFirst(E tupleFirst) { this.tupleFirst = tupleFirst; }

    /**
     * Metoda seteaza o valoare celui de-al doilea element din tuplu.
     * @param tupleSecond E
     */
    public void setSecond(E tupleSecond) { this.tupleSecond = tupleSecond; }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Tuple<?>)) return false;
        boolean firstCondition = (this.tupleFirst.equals(((Tuple<?>)obj).tupleFirst) && this.tupleSecond.equals(((Tuple<?>)obj).tupleSecond));
        boolean secondCondition = (this.tupleFirst.equals(((Tuple<?>)obj).tupleSecond) && this.tupleSecond.equals(((Tuple<?>)obj).tupleFirst));
        return firstCondition || secondCondition;
    }

    @Override
    public int hashCode() {
        if (getFirstTupleElement() == null || getFirstTupleElement().compareTo(getSecondTupleElement()) < 0)
            return super.hashCode();
        return Objects.hash(getSecondTupleElement(), getFirstTupleElement());
    }

    @Override
    public String toString(){ return getFirstTupleElement() + "," + getSecondTupleElement(); }
}
