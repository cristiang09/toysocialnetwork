package ui;
import domain.Friendship;
import domain.Tuple;
import domain.User;
import domain.dto.UserDTO;
import domain.validators.ValidareException;
import domain.validators.ValidareFriendship;
import domain.validators.ValidareUser;
import domain.validators.Validator;
import repository.Repository;
import repository.database.friendship.FriendshipDbRepository;
import repository.database.user.UserDbRepository;
import service.ServiceUF;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Main {
    public static void main(String[] args) {
        Repository<Long, User> repositoryUserMain = new UserDbRepository("jdbc:postgresql://localhost:5432/laborator3MAP", "postgres", "ccrest2016*");
        Repository<Tuple<Long>, Friendship> RepoFriendship = new FriendshipDbRepository("jdbc:postgresql://localhost:5432/laborator3MAP", "postgres", "ccrest2016*");
        ServiceUF serviceUF = new ServiceUF(RepoFriendship, repositoryUserMain);
        String meniu = "\n\n1. AdaugaUser\n2. AdaugaPrietenie\n3. StergeUser\n4. StergePrietenie\n5. NumarDeComunitati\n6. CeaMaiSociabilaComunitate\n7. AfisareUser\n8. UpdateUser.\n9. Exit";
        while (true)
        {
            System.out.println(meniu);
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            try {
                System.out.println("Comanda este-> ");
                String command = reader.readLine();
                List<String> attr = new ArrayList<>();
                if(command.equals("9")) break;
                switch (command) {
                    case "1" -> {
                        System.out.println("Firstname user-> ");
                        attr.add(reader.readLine());
                        System.out.println("Lastname user-> ");
                        attr.add(reader.readLine());
                        serviceUF.addUser(attr);
                        System.out.println("Adaugarea s-a realizat cu succes!");
                    }
                    case "2" -> {
                        System.out.println("ID user1-> ");
                        attr.add(reader.readLine());
                        System.out.println("ID user2-> ");
                        attr.add(reader.readLine());
                        serviceUF.addFriendship(attr);
                        System.out.println("Adaugarea s-a realizat cu succes!");
                    }
                    case "3" -> {
                        System.out.println("ID user-> ");
                        String id = reader.readLine();
                        serviceUF.removeUser(Long.parseLong(id));
                        System.out.println("Stergerea s-a realizat cu succes!");
                    }
                    case "4" -> {
                        System.out.println("ID user1-> ");
                        String id1 = reader.readLine();
                        System.out.println("ID user2-> ");
                        String id2 = reader.readLine();
                        Tuple<Long> tuple = new Tuple<>(Long.parseLong(id1), Long.parseLong(id2));
                        serviceUF.removeFriendship(tuple);
                        System.out.println("Stergerea s-a realizat cu succes!");
                    }
                    case "5" -> System.out.println("Numarul de comunitati este: " + serviceUF.numberOfConnectedComponents());
                    case "6" -> {
                        System.out.println("Cea mai sociabila comunitate este formata din " + serviceUF.maxConnectedComponent().size() + " utilizatori!");
                        for (User user : serviceUF.maxConnectedComponent()){
                            UserDTO userDTO = new UserDTO(user.getFirstName(), user.getLastName());
                            System.out.print("  --->  " + userDTO.toString());
                        }
                    }
                    case "7" -> serviceUF.getAllUsers().forEach(System.out::println);
                    case "8" -> {
                        System.out.println("Id-ul user: ");
                        String id1 = reader.readLine();
                        System.out.println("Firstname user-> ");
                        String first_name = reader.readLine();
                        System.out.println("Lastname user-> ");
                        String last_name = reader.readLine();
                        serviceUF.updateUser(Long.parseLong(id1), first_name, last_name);
                    }
                }
            } catch (ValidareException | NumberFormatException | IOException e){
                System.out.println(e.getMessage());
            }
        }

    }

 }
