package repository.memory;
import domain.Entity;
import repository.Repository;

import java.util.HashMap;
import java.util.Map;

public class InMemoryRepository <ID, E extends Entity<ID>> implements Repository<ID, E> {

    protected Map<ID, E> entities;

    /**
     * Constructorul clasei InMemoryRepository
     */
    public InMemoryRepository() {
        this.entities = new HashMap<ID, E>();
    }

    @Override
    public E findOne(ID Id)
    {
        if (Id == null) throw new IllegalArgumentException("Id must be not null!");
        return entities.get(Id);
    }

    @Override
    public Iterable<E> findAll() {
        return entities.values();
    }

    @Override
    public E save(E entity) {
        if (entity == null) throw new IllegalArgumentException("Entitiy must not be null!");
        if (entities.get(entity.getId()) != null) return entity;
        else entities.put(entity.getId(), entity);
        return null;
    }

    @Override
    public E delete(ID Id) {
        if (Id == null) throw new IllegalArgumentException("Id must not be null!");
        if(findOne(Id) != null) {
            E returnEntity = entities.get(Id);
            entities.remove(Id);
            return returnEntity;
        }
        return null;
    }

    @Override
    public void update(E entity) {

    }

}
