package repository;

import domain.Entity;

public interface Repository<ID, E extends Entity<ID>> {
    /**
     * Metoda cauta un anumit obiect dupa id.
     * @param Id ID
     * @return E, daca entitatea exista
     *         null, daca entitatea nu exista
     * @throws IllegalArgumentException, daca id-ul este null
     */
    E findOne(ID Id);

    /**
     * @return Iterable<E>, lista de valori E din map-ul.
     */
    Iterable<E> findAll();

    /**
     * Metoda salveaza entitatea in repository.
     * @param entity E, entitea salvata
     * @return E, daca entitatea exista
     *         null, daca entitatea nu exista si este adaugata
     * @throws IllegalArgumentException, daca entitatea este null
     */
    E save(E entity);

    /**
     * Metoda sterge o entitate dupa id
     * @param Id ID
     * @return null daca stergerea se realizeaza
     *         E, entitatea care a fost stearsa
     * @throws IllegalArgumentException, in cazul in care Id-ul este null
     */
    E delete(ID Id);

    void update(E entity);
}
