package repository.database.friendship;

import domain.Friendship;
import domain.Tuple;
import repository.Repository;
import domain.User;
import java.util.Date;

import java.sql.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class FriendshipDbRepository implements Repository<Tuple<Long>, Friendship> {
    private String url;
    private String username;
    private String password;

    public FriendshipDbRepository(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public Friendship findOne(Tuple<Long> Id) {
        Friendship friendship = null;
        String sql = "select * from friendships where id_user1 = '" + Id.getFirstTupleElement() + "' and  id_user2='" + Id.getSecondTupleElement()+"' or id_user1 ='"+ Id.getSecondTupleElement() + "' and id_user2='" + Id.getFirstTupleElement() +"'";
        try (Connection connection = DriverManager.getConnection(this.url, this.username, this.password);
             PreparedStatement statement = connection.prepareStatement(sql );
             ResultSet resultSet = statement.executeQuery();
        )
        {
            while (resultSet.next()) {
                String date = resultSet.getString("date");
                friendship = new Friendship(date);
                friendship.setId(Id);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return friendship;
    }

    @Override
    public Iterable<Friendship> findAll()
    {
        Set<Friendship> friendships = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(this.url, this.username, this.password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from friendships");
             ResultSet resultSet = statement.executeQuery())
        {
            while (resultSet.next())
            {
                Long id1 = resultSet.getLong("id_user1");
                Long id2 = resultSet.getLong("id_user2");
                String date = resultSet.getString("date");
                Friendship friendship = new Friendship(date);
                friendship.setId(new Tuple<>(id1, id2));
                friendships.add(friendship);
            }
            return friendships;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return friendships;
    }

    @Override
    public Friendship save(Friendship entity) {
        String sql = "insert into friendships (id_user1, id_user2, date) values (?, ?, ?)";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, entity.getId().getFirstTupleElement());
            ps.setLong(2, entity.getId().getSecondTupleElement());
            ps.setString(3, entity.getDateFriendship());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Friendship delete(Tuple<Long> Id) {
        String sql = "delete from friendships where id_user1 = ? and id_user2 = ? or id_user1 = ? and id_user2 = ?";
        try (Connection connection = DriverManager.getConnection(this.url, this.username, this.password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql))
        {
            preparedStatement.setLong(1, Id.getFirstTupleElement());preparedStatement.setLong(2, Id.getSecondTupleElement());
            //preparedStatement.executeUpdate();
            preparedStatement.setLong(3, Id.getSecondTupleElement());preparedStatement.setLong(4, Id.getFirstTupleElement());
            preparedStatement.executeUpdate();

        }
        catch (SQLException e) { e.printStackTrace(); }
        return null;
    }

    @Override
    public void update(Friendship entity) {

    }

}
