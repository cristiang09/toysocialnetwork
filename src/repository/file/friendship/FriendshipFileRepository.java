package repository.file.friendship;

import domain.Friendship;
import domain.Tuple;
import repository.file.AbstractFileRepository;
import java.util.List;

public class FriendshipFileRepository extends AbstractFileRepository<Tuple<Long>, Friendship> {

    /**
     * Constructorul clasei FriendshipFileRepository
     * @param fileName String
     */
    public FriendshipFileRepository(String fileName) {
        super(fileName);
    }

    @Override
    public Friendship extractEntity(List<String> attributes) {
        Friendship friendship = new Friendship("");
        Tuple<Long> tupleId = new Tuple<>(Long.parseLong(attributes.get(0)), Long.parseLong(attributes.get(1)));
        friendship.setId(tupleId);
        return friendship;
    }

    @Override
    public String createEntityAsString(Friendship friendship) {
        return friendship.getId().getFirstTupleElement() + ";" + friendship.getId().getSecondTupleElement();
    }
}
