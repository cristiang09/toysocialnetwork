package repository.file.user;

import domain.User;
import repository.file.AbstractFileRepository;
import java.util.List;

public class UserFileRepository extends AbstractFileRepository<Long, User> {

    /**
     * Constructorul clasei UserFileRepository
     * @param fileName String
     */
    public UserFileRepository(String fileName) {
        super(fileName);
    }

    @Override
    public User extractEntity(List<String> attributes) {
        User user = new User(attributes.get(1), attributes.get(2));
        user.setId(Long.parseLong(attributes.get(0)));
        return user;
    }

    @Override
    public String createEntityAsString(User user) {
        return user.getId().toString() + ";" + user.getFirstName() + ";" + user.getLastName();
    }
}
