package repository.file;

import domain.Entity;
import repository.memory.InMemoryRepository;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractFileRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID, E> {
    private final String fileName;

    /**
     * Constructorul clasei AbstractFileRepository
     * @param fileName String
     */
    public AbstractFileRepository(String fileName){
        this.fileName = fileName;
        loadFromFile();
    }

    /**
     * Metoda incarca din fisier in memorie entitatiile.
     */
    private void loadFromFile()
    {
        Path path = Paths.get(this.fileName);
        try{
            List<String> lines = Files.readAllLines(path);
            lines.forEach(line->{
                E entity = extractEntity(Arrays.asList(line.split(";")));
                entities.put(entity.getId(), entity);
            });
        }
        catch(IOException e) {e.printStackTrace();}
    }

    /**
     * Metoda insereaza in fisier o entitate
     * @param entity E
     */
    protected void writeToFile(E entity)
    {
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(this.fileName, true)))
        {
            bufferedWriter.write(createEntityAsString(entity));
            bufferedWriter.newLine();
        }
        catch(IOException e){e.printStackTrace();}
    }

    @Override
    public E save(E entity)
    {
        E result = super.save(entity);
        if (result == null) writeToFile(entity);
        return entity;
    }

    @Override
    public E delete(ID Id)
    {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(this.fileName));
            bufferedWriter.write("");
            E result = super.delete(Id);
            if (result != null) findAll().forEach(this::writeToFile);
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Metoda creeaza o entitate de tip E dintr-o lista de string-uri
     * @param attributes List<String>
     * @return E, entitatea creata
     */
    public abstract E extractEntity(List<String> attributes);

    /**
     * Metoda creeaza dintr-o entitate, un string(modul in care este reprezentat o entitate in fisier)
     * @param entity E
     * @return String
     */
    public abstract String createEntityAsString(E entity);
}
